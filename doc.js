#!/usr/bin/env node

const _ = require('lodash');
const P = require('bluebird');
const fs = require('fs');
const ejs = require('ejs');
const path = require('path');
const apidoc = require('apidoc');
const commander = require('commander');
const Turndown = require('turndown');
const logger = require('@comodinx/logger');
const pkg = require('./package.json');

const turndown = new Turndown();

// Command
const program = commander
    .version(pkg.version)
    .description('Auto generate README documentation file.')
    .command('./doc.js')
    .usage('[options]')
    .option('-n, --name <path>', 'Application name.')
    .option('-g, --group <path>', 'Group application name.')
    .option('-m, --markdown <path>', 'Default markdown file path.')
    .option('-d, --dirname <path>', 'Application dirname.', process.cwd())
    .parse(process.argv);

// Arguments
// Arguments :: Required
try {
    if (!fs.statSync(program.dirname).isDirectory()) {
        throw new Error(`Please set a valid dirname [${program.dirname}]`);
    }
}
catch (e) {
    logger.error(e.message);
    program.help();
    process.exit(0);
}

// Arguments :: Optionals
program.name = program.name || path.basename(process.cwd());

if (!program.name) {
    logger.error('Please set Application name');
    program.help();
    process.exit(0);
}

if (!program.group) {
    logger.error('Please set Group application name');
    program.help();
    process.exit(0);
}

try {
    if (program.markdown && !fs.statSync(program.markdown).isFile()) {
        throw new Error(`Please set a valid Markdown file path [${program.markdown}]`);
    }
}
catch (e) {
    logger.error(e.message);
    program.help();
    process.exit(0);
}

// Functions
function loadNecesaryFiles () {
    logger.step('Loading necesary files');

    return P.resolve()
        .then(() => {
            program.pkg = require(`${program.dirname}/package.json`);
            return program.pkg;
        })
        .then(() => logger.success('Necesary files loaded!'));
}

function createApiDirs () {
    logger.step('Creating required APIDoc directory');

    return new P((resolve, reject) => {
        const recursive = true;

        program.apidocDirname = `${program.dirname}/doc`;

        fs.mkdir(program.apidocDirname, { recursive }, error => {
            if (error) return reject(error);
            logger.success('APIDoc directory is already created!');
            return resolve();
        });
    });
}

function createApiFiles () {
    logger.step('Creating required APIDoc files');

    return new P((resolve, reject) => {
        const data = _.defaults({}, program.pkg.apidoc || {}, {
            name: program.pkg.name,
            version: program.pkg.version,
            description: program.pkg.description,
            title: program.pkg.name,
            url: program.pkg.homepage
        });

        program.apidocFile = `${program.dirname}/apidoc.json`;

        fs.writeFile(program.apidocFile, JSON.stringify(data, null, 2), error => {
            if (error) return reject(error);
            logger.success('APIDoc file is already created!');
            return resolve();
        });
    });
}

function createApiDefaults () {
    logger.step('Creating defaults APIDoc files');

    return new P((resolve, reject) => {
        const data = [];

        data.push('/**');
        data.push(' * @api {get} /health Health check.');
        data.push(' * @apiVersion 0.0.1');
        data.push(' * @apiName GetHealth');
        data.push(' * @apiGroup Health');
        data.push(' *');
        data.push(` * @apiSampleRequest ${program.pkg.name.toLowerCase()}${program.group ? `.${program.group}` : ''}.com/health`);
        data.push(' *');
        data.push(' * @apiSuccess {String}   name               Application name.');
        data.push(' * @apiSuccess {String}   id                 Application id.');
        data.push(' * @apiSuccess {String}   version            Application version.');
        data.push(' * @apiSuccess {String}   description        Application description.');
        data.push(' * @apiSuccess {String}   environment        Environment name.');
        data.push(' * @apiSuccess {Boolean}  alive              Boolean indicate if application is alive and full operational.');
        data.push(' * @apiSuccess {Object}   database           Database status.');
        data.push(' * @apiSuccess {Boolean}  database.alive     Boolean indicate if database is alive.');
        data.push(' * @apiSuccess {Object}   [database.error]   Database error.');
        data.push(' * @apiSuccess {Object}   dependencies       Services dependencies status.');
        data.push(' * @apiSuccess {Boolean}  dependencies.alive Boolean indicate if services dependecies are alive and full operational.');
        data.push(' * @apiSuccess {Object[]} dependencies.list  List of the status of each dependency. (Result of the health check of each one of them)');
        data.push(' * @apiSuccessExample {json} Success-Response:');
        data.push(' *     // HTTP/1.1 200 OK');
        data.push(' *     {');
        data.push(' *       "environment": "development",');
        data.push(` *       "name": "${program.pkg.name}",`);
        data.push(` *       "description": "${program.pkg.description}",`);
        data.push(' *       "version": "0.0.1",');
        data.push(' *       "alive": true,');
        data.push(' *       "database": {');
        data.push(' *         "alive": true');
        data.push(' *       }');
        data.push(' *       "dependencies": {');
        data.push(' *         "alive": true,');
        data.push(' *         "list": [{');
        data.push(' *           "alive": true,');
        data.push(' *           "name": "UsersApplication",');
        data.push(' *           "environment": "testing",');
        data.push(' *           "description": "Users Application is a logins administrator.",');
        data.push(' *           "version": "0.0.1",');
        data.push(' *           "database": {');
        data.push(' *             "alive": true');
        data.push(' *           },');
        data.push(' *           "dependencies": ...');
        data.push(' *         }, {');
        data.push(' *           "name": "OTPApplication",');
        data.push(' *           "alive": true,');
        data.push(' *           "error": {');
        data.push(' *             "code": 500,');
        data.push(' *             "message": "Internal Server Error"');
        data.push(' *           }');
        data.push(' *         }]');
        data.push(' *       }');
        data.push(' *     }');
        data.push(' */');

        data.push('/**');
        data.push(' * @apiDefine DefaultError');
        data.push(' * @apiError {Integer} code    Error status code');
        data.push(' * @apiError {String}  error   Error message');
        data.push(' * @apiError {Object}  [extra] Additional information');
        data.push(' * @apiErrorExample {json} XXX (Error)');
        data.push(' *     {');
        data.push(' *       "code": XXX,');
        data.push(' *       "error": "<message>"');
        data.push(' *     }');
        data.push(' *     // Example: { "code": 404, "error": "Not Found" }');
        data.push(' */');
        data.push('');

        program.apidocDefaultsFile = `${program.dirname}/core/apidoc.js`;

        fs.writeFile(program.apidocDefaultsFile, data.join('\n'), error => {
            if (error) return reject(error);
            logger.success('Default APIDoc file is already created!');
            return resolve();
        });
    });
}

function createApiDoc () {
    logger.step('Creating documentation');

    return new P((resolve, reject) => {
        const options = {
            src: `${program.dirname}/core`,
            dest: program.apidocDirname,
            // debug: true
        };

        if (apidoc.createDoc(options) === false) {
            return reject(new Error('API doc not generated'));
        }
        logger.success('Documentation is already created!');
        return resolve();
    });
}

function createApiMarkdown () {
    logger.step('Creating README.md');

    return new P((resolve, reject) => {
        try {
            const template = ejs.compile(fs.readFileSync(program.markdown || `${__dirname}/doc.md`).toString());
            const apiData = JSON.parse(fs.readFileSync(`${program.apidocDirname}/api_data.json`));
            const project = JSON.parse(fs.readFileSync(`${program.apidocDirname}/api_project.json`));
            const api = _.groupBy(_.filter(apiData, data => data.type), 'group');
            const data = {};
            const locals = {
                undef: obj => obj || '',
                mlink: obj => (obj || '').toLowerCase().replace(/\s/g, '-'),
                upcase: obj => (obj || '').toUpperCase(),
                project: _.merge(project, {
                    group: program.group ? `/${program.group}` : ''
                }),
                turndown,
                data,
                _
            };

            _.each(_.keys(api), key => {
                data[key] = _.groupBy(api[key], 'name');
                return data[key];
            });

            if (project.order) {
                locals.data = sortByOrder(data, project.order);
            }

            fs.writeFile(`${program.dirname}/README.md`, template(locals), error => {
                if (error) return reject(error);
                logger.success('README.md is already created!');
                return resolve();
            });
        }
        catch (e) {
            logger.error(e.message);
            reject(e);
        }
    });
}

function sortByOrder (groups, order, recursive = true) {
    const resultGroups = {};

    _.each(order, name => {
        _.each(groups, (group, keyGroup) => {
            if (keyGroup === name) {
                resultGroups[keyGroup] = recursive ? sortByOrder(group, order, false) : group;
            }
        });
    });

    // Append all other entries that ar not defined in order
    _.each(groups, (group, keyGroup) => {
        if (!resultGroups[keyGroup]) {
            resultGroups[keyGroup] = recursive ? sortByOrder(group, order, false) : group;
        }
    });
    return resultGroups;
}

function cleanAll () {
    logger.step('Cleaning all directory');

    return P.resolve()
        // .then(() => {
        //     return new P((resolve, reject) => {
        //         fs.unlink(program.apidocFile, error => {
        //             if (error) return reject(error);
        //             return resolve();
        //         });
        //     });
        // })
        // .then(() => {
        //     return new P((resolve, reject) => {
        //         fs.unlink(program.apidocDefaultsFile, error => {
        //             if (error) return reject(error);
        //             return resolve();
        //         });
        //     });
        // })
        .then(() => {
            return new P((resolve, reject) => {
                const recursive = true;

                fs.rmdir(program.apidocDirname, { recursive }, error => {
                    if (error) return reject(error);
                    return resolve();
                });
            });
        })
        .then(() => logger.success('Directory is already cleaned!'))
        .catch(log);
}

function log (error) {
    logger.error(error.stack);
}

// Source code
P.resolve()
    .then(() => loadNecesaryFiles())
    .then(() => createApiDirs())
    .then(() => createApiFiles())
    .then(() => createApiDefaults())
    .then(() => createApiDoc())
    .then(() => createApiMarkdown())
    // Not need
    // . then(() => createSwaggerDoc())
    .catch(log)
    .finally(() => cleanAll());

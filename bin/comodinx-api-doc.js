#!/usr/bin/env node

// Prevent unexpected errors
process.once('uncaughtException', err => {
    console.error(err.stack);
    // Default error response code 1 (failure)
    process.exitCode = 1;
});

// TODO Handle response code
require('../doc');

// Default response code 0 (success)
process.exitCode = 0;

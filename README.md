# API Doc generator

@comodinx/api-doc is a Node.js API Doc generator.

This module will be in charge of assembling the README corresponding to the specified documentation, in [APIDocJs](https://apidocjs.com/) format, on your controllers/routes/endpoints.

## Index

* [Download & Install][install].
* [How is it used?][how_is_it_used].

## Download & Install

### NPM
```bash
    npm install @comodinx/api-doc
```

### Source code
```bash
$ git clone https://gitlab.com/comodinx/api-doc.git
$ cd api-doc
$ npm install
```

## How is it used?

### Configure

In your `package.json`, set next script

```js
{
  ...
  "scripts": {
    "doc": "comodinx-api-doc",
    ...
  }
  ...
}
```

### Options

| Argument | Alias | Value     | Default value              | Description                                             |
|:---------|:------|:----------|:---------------------------|:--------------------------------------------------------|
| markdown | m     | file path | [doc.md](./doc.md)         | Default markdown file path.                             |
| group    | g     | path      | ''                         | Group application name. (Gitlab/Github group/user name) |
| dirname  | d     | path      | current cwd directory      | Application dirname.                                    |
| name     | n     | path      | Base name for current path | Application name.                                       |
| help     | h     | boolean   | `false`                    | Output usage information.                               |

### Examples

#### Custom markdown

Use a custom README markdown.

##### Custom markdown :: Case 1:

Directly on package.json script.

```js
{
  ...
  "scripts": {
    "doc": "comodinx-api-doc -m ./README.template.md",
    ...
  }
  ...
}
```

##### Custom markdown :: Case 2:

By command line.

```js
$ npm run doc -- -m ./README.template.md
```

### Next?

Enjoy the magic and I owe you the documentation

<!-- deep links -->
[install]: #download--install
[how_is_it_used]: #how-is-it-used

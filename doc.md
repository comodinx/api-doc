# <%= project.name %>

<%= project.description %>

| **TESTING** | **STAGING/QA** | **PRODUCTION** |
|:-----------:|:--------------:|---------------:|
| [![pipeline status](https://gitlab.com<%= project.group %>/<%= project.name %>/badges/develop/pipeline.svg)](https://gitlab.com<%= project.group %>/<%= project.name %>/-/commits/develop) | [![pipeline status](https://gitlab.com<%= project.group %>/<%= project.name %>/badges/staging/pipeline.svg)](https://gitlab.com<%= project.group %>/<%= project.name %>/-/commits/staging) | [![pipeline status](https://gitlab.com<%= project.group %>/<%= project.name %>/badges/master/pipeline.svg)](https://gitlab.com<%= project.group %>/<%= project.name %>/-/commits/master) |

## Index

* [Download][download].
* [Database][database].
* [How is it used?][how_is_it_used].
* [Endpoints][endpoints].
* [Tests][tests].

## Download

### Source code
```shell
$ git clone git@gitlab.com:<%= project.group %>/<%= project.name %>.git
$ cd <%= project.name %>
$ npm install
```

## Database

Each microservice has its own database. Here we show you some useful commands for the database.

### Install

```shell
# Install database on local environment, with defaults connection credentials.
npm run db:install
#
# Install database with custom connection credentials.
npm run db:install -- -h=docker.mysql.host -P=3307 -u=admin -p=1234abcd
#
# For more details, run
npm run db:install -- --help
```

### Release

```shell
# Release database on local environment, with defaults connection credentials.
npm run db:release -- --release=0_0_1
#
# Rollback release database on local environment, with defaults connection credentials.
npm run db:release -- --release=0_0_1 --rollback
#
# Release database with custom connection credentials.
npm run db:release -- --release=0_0_1 -h=docker.mysql.host -P=3307 -u=admin -p=1234abcd
#
# For more details, run
npm run db:release -- --help
```

## How is it used?

### Run

Create a .env file or export the required environment variables based on the .env.* files and then run one of the following commands:

```shell
# Development
npm run dev
#
# Development - Watch changes
npm run dev:watch
#
# Production
npm start
#
# Docker
docker-compose up -d --build
```

> NOTE: Append `-win` for running command for Windows SO. Example: `npm run dev-win`

## Endpoints
<% _.each(data, group => { -%>
<% _.each(group, subgroup => { -%>
<% subgroup = subgroup[0]; -%>
* `<%= subgroup.type.toUpperCase() %>` [<%= subgroup.url %>](#<%= subgroup.type.toLowerCase() %>-<%= subgroup.url.replace(/:|\//g, '').toLowerCase() %>) <%= subgroup.title %>
<% }); -%>
<% }); -%>
<% _.each(data, group => { -%>
<% _.each(group, subgroup => { -%>
<% subgroup = subgroup[0]; -%>

#### <%= subgroup.type.toUpperCase() %> `<%= subgroup.url %>`

<%= subgroup.title %>
<% if (subgroup.header && subgroup.header.fields) { -%>

##### Request Headers
| Name         | Type      | Description                          |
|:-------------|:----------|:-------------------------------------|
<% _.each(subgroup.header.fields, headers => { -%>
<% _.each(headers, header => { -%>
| <%- header.field %>     | <%- header.type %>      | <%- header.optional ? '_**(Optional)**_' : '' %> <%- turndown.turndown(header.description) %>              |
<% }); -%>
<% }); -%>
<% } -%>
<% if (subgroup.parameter && subgroup.parameter.fields) { -%>
##### Request Parameters
| Name         | Type      | Group      | Description                          |
|:-------------|:----------|:-----------|:-------------------------------------|
<% _.each(subgroup.parameter.fields, params => { -%>
<% _.each(params, param => { -%>
| <%- param.field %>      | <%- param.type %>     | <%- param.group %> | <%- param.optional ? '_**(Optional)**_' : '' %> <%- turndown.turndown(param.description) %><%- param.defaultValue ? `. Default value is \`${param.defaultValue}\`` : '' %> |
<% }); -%>
<% }); -%>
<% } -%>
<% if (subgroup.examples && subgroup.examples.length) { -%>

##### Request Examples
<% _.each(subgroup.examples, example => { -%>
###### <%- example.title %>:

```<%- example.type %>
<%- example.content %>
```
<% }); -%>
<% } -%>
<% if (subgroup.success) { -%>
<% if (subgroup.success.fields && subgroup.success.fields['Success 200'] && subgroup.success.fields['Success 200'].length) { -%>

##### Response Body (Success)
| Name         | Type      | Description                          |
|:-------------|:----------|:-------------------------------------|
<% _.each(subgroup.success.fields['Success 200'], param => { -%>
| <%- param.field %>      | <%- param.type %>     | <%- param.optional ? '_**(Optional)**_' : '' %> <%- turndown.turndown(param.description) %><%- param.defaultValue ? `. Default value is \`${param.defaultValue}\`` : '' %> |
<% }); -%>
<% } -%>
<% if (subgroup.success.examples && subgroup.success.examples.length) { -%>

<% _.each(subgroup.success.examples, example => { -%>
##### Response <%- example.title === 'Success-Response:' ? '200 (OK)' : example.title %>:

```json
<%- example.content %>
```
<% }); -%>
<% } -%>
<% } -%>
<% if (subgroup.error) { -%>
<% if (subgroup.error.fields && subgroup.error.fields['Error 4xx'] && subgroup.error.fields['Error 4xx'].length) { -%>

##### Response Body (Error)
| Name         | Type      | Description                          |
|:-------------|:----------|:-------------------------------------|
<% _.each(subgroup.error.fields['Error 4xx'], param => { -%>
| <%- param.field %>      | <%- param.type %>     | <%- param.optional ? '_**(Optional)**_' : '' %> <%- turndown.turndown(param.description) %><%- param.defaultValue ? `. Default value is \`${param.defaultValue}\`` : '' %> |
<% }); -%>
<% } -%>
<% if (subgroup.error.examples && subgroup.error.examples.length) { -%>

<% _.each(subgroup.error.examples, example => { -%>
##### Response <%- example.title === 'Error-Response:' ? 'XXX (Error)' : example.title %>:

```json
<%- example.content %>
```
<% }); -%>
<% } -%>
<% } -%>
<% }); -%>
<% }); -%>

## Tests

In order to see more concrete examples, **I INVITE YOU TO LOOK AT THE TESTS :)**

### Run the unit tests
```shell
npm test
```

<!-- deep links -->
[download]: #download
[database]: #database
[how_is_it_used]: #how-is-it-used
[endpoints]: #endpoints
[tests]: #tests
